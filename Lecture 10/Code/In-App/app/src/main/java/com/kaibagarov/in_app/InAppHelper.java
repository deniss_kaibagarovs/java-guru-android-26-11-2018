package com.kaibagarov.in_app;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.kaibagarov.in_app.inApp.IabHelper;
import com.kaibagarov.in_app.inApp.IabResult;

public class InAppHelper {

    private static String base64EncodedPublicKey = "";
    private static String productID = "remove.ads";
    static final int RC_REQUEST = 10001;

    IabHelper mHelper;
    Context context;

    public InAppHelper(Context context) {
        this.context = context;
        mHelper = new IabHelper(this.context, base64EncodedPublicKey);

        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {

                if (!result.isSuccess()) {
                    Log.d("", "Setup finished with error");
                    return;
                }

                Log.d("", "Setup finished");
            }
        });
    }

    public void purchaseRemoveADS(IabHelper.OnIabPurchaseFinishedListener listener) {
        try {
            String payload = "";

            mHelper.launchPurchaseFlow((Activity)this.context, productID, RC_REQUEST,
                    listener, payload);
        } catch (Exception e) {
            Log.v("",e.getLocalizedMessage());
        }
    }
}
