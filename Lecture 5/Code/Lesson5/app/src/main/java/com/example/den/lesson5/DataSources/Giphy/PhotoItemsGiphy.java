package com.example.den.lesson5.DataSources.Giphy;

import com.example.den.lesson5.Interfaces.PhotoItem;

import java.util.Map;

public class PhotoItemsGiphy implements PhotoItem {

    ImagesContainer images;
    String username;

    @Override
    public String getImgUrl() {
        return images.getMediumSize();
    }

    @Override
    public String getAuthorName() {
        return username;
    }

    class ImagesContainer {
        Map<String,String> downsized_medium;

        public String getMediumSize() {
            return downsized_medium.get("url");
        }
    }
}
